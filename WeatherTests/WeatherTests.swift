//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by Jumpei Katayama on 10/26/14.
//  Copyright (c) 2014 Jumpei Katayama. All rights reserved.
//

import UIKit
import XCTest
import CoreLocation
@testable import Weather

class WeatherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFtoC() {
        let celsius =  WeatherHelper.convertFtoC(86)
        XCTAssertEqual(celsius, 30)
    }
    
    func testConvertGeolocationToCity() {
        let expectation = expectationWithDescription("testConvertGeolocationToCity")
        let location = CLLocation(latitude: 37.71988628, longitude: 122.48217857)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            guard error == nil else { return }
            
            let myPlacemark = placemarks![0].locality!
            XCTAssertEqual(myPlacemark, "San Francisco")
            expectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(5, handler: nil)
    }
    
    func testDownloadWeatherData() {
        let geoLocation = CLLocationCoordinate2D(latitude: 37.71988628, longitude: 122.48217857)
        let exceptation = expectationWithDescription("downloadWeatherData")
        WeatherHelper.getCurrentWeatherData(geoLocation, completion: {(location, repsponse, error) in
            guard error == nil else {// Error handling
                log.error("FAIL: Download wewther information")
                return
            }
            
            let dataObject = NSData(contentsOfURL: location!)
            log.info("SUCESS: Download weather information")
            do {
                let weatherDictionary: NSDictionary = try NSJSONSerialization.JSONObjectWithData(dataObject!, options: []) as! NSDictionary
                let currentWeather = Current(weatherDictionary: weatherDictionary)
                XCTAssertNotNil(currentWeather)
                exceptation.fulfill()
            } catch {
                log.error("FAIL: Create wether dictionary from JSON")
                print(error)
            }
        })
        waitForExpectationsWithTimeout(5, handler: nil)
    }
    
    
}
