//
//  Current.swift
//  Weather
//
//  Created by Jumpei Katayama on 10/26/14.
//  Copyright (c) 2014 Jumpei Katayama. All rights reserved.
//

import Foundation
import UIKit

struct Current {
    
    var currentTime: String?
    var temperature: Int
    var temperature_celsius: Int
    var humidity: Double
    var precipProbability: Double
    var summary: String
    var cloudCover: Double
    var dewPoint: Double
    var ozone: Double
    var pressure: Double
    var windSpeed: Double
    var sunriseTime: String?
    var sunsetTime: String?
    var windBearing: Int
    var icon: UIImage?
    
    init(weatherDictionary: NSDictionary) {
        let currentWeather = weatherDictionary["currently"] as! NSDictionary
        temperature = currentWeather["temperature"] as! Int
        temperature_celsius = Int(WeatherHelper.convertFtoC(currentWeather["temperature"] as! Int))
        humidity = currentWeather["humidity"] as! Double
        precipProbability = currentWeather["precipProbability"] as! Double
        summary = currentWeather["summary"] as! String
        cloudCover = currentWeather["cloudCover"] as! Double
        dewPoint = currentWeather["dewPoint"] as! Double
        ozone = currentWeather["ozone"] as! Double
        pressure = currentWeather["pressure"] as! Double
        windSpeed = currentWeather["windSpeed"] as! Double
        windBearing = currentWeather["windBearing"] as! Int
//        let sunsetTimeIntValue = currentWeather["sunsetTime"] as! Int
//        let sunriseTimeTimeIntValue = currentWeather["sunriseTime"] as! Int
        let currentTimeIntValue = currentWeather["time"] as! Int
        
        currentTime = dateStringFromUnixtime(currentTimeIntValue)
//        sunriseTime = dateStringFromUnixtime(sunriseTimeTimeIntValue)
//        sunsetTime = dateStringFromUnixtime(sunsetTimeIntValue)

        let iconString = currentWeather["icon"] as! String
        icon = weatherIconFromString(iconString)
        
    }
    
    /**
        Return current time with string from int
    
    */
    private func dateStringFromUnixtime(unixTime: Int) -> String {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter.stringFromDate(weatherDate)
    }
    
    /**
        icon image can be changed depending on the current weather
    
    */
    func weatherIconFromString(stringIcon: String) -> UIImage {
        var imageName: String
        switch stringIcon {
        case "clear-day":
            imageName = "clear-day"
        case "clear-night":
            imageName = "clear-night"
        case "rain":
            imageName = "rain"
        case "snow":
            imageName = "snow"
        case "sleet":
            imageName = "sleet"
        case "wind":
            imageName = "wind"
        case "fog":
            imageName = "fog"
        case "cloudy":
            imageName = "cloudy"
        case "partly-cloudy-day":
            imageName = "partly-cloudy"
        case "partly-cloudy-night":
            imageName = "cloudy-night"
        default:
            imageName = "default"
        }
        let iconName = UIImage(named: imageName)
        return iconName!
    }
    
    
}