//
//  WeatherView.swift
//  Weather
//
//  Created by Jumpei Katayama on 10/7/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var sunRiseTimeLabel: UILabel!
    @IBOutlet weak var sunSetTimeLabel: UILabel!
    @IBOutlet weak var minTepertureLabel: UILabel!
    @IBOutlet weak var maxTepertureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weather_celsius: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var refresh: UIButton!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    func updateUI(newdata: Current? = nil) {
        guard let current = newdata else {
            indicator.stopAnimating()
            indicator.hidden = true
            refresh.hidden = false
            return
        }
        
        iconView.image = current.icon!
        currentTimeLabel.text = "\(current.currentTime!)"
        humidityLabel.text = "\(current.humidity*100)%"
        precipitationLabel.text = "\(current.precipProbability*100)%"
        summaryLabel.text = current.summary
        weatherLabel.text = "\(current.temperature)ºF"
        weather_celsius.text = "\(current.temperature_celsius)ºC"
        windSpeedLabel.text = "windSpeed: \(current.windSpeed)"
        sunRiseTimeLabel.text = "sunrise: \(current.sunriseTime)"
        sunSetTimeLabel.text = "sunset: \(current.sunsetTime)"
        
        indicator.stopAnimating()
        indicator.hidden = true
        refresh.hidden = false
    }
    
    func updateCityName(name: String!) {
        cityNameLabel.text = name
    }
    
    
}
