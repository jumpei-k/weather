//
//  WeatherHelper.swift
//  Weather
//
//  Created by Jumpei Katayama on 1/2/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import XCGLogger



private let apikey = "30d24c3b4cb112f677c2d9038783076d"

class WeatherHelper {
    typealias downloadCompletion = (NSURL?, NSURLResponse?, NSError?) -> Void
    
    static func getCurrentWeatherData(coordinate: CLLocationCoordinate2D, completion: downloadCompletion) {
        log.info("Checking api url...")
        let coordinate = coordinateAsStringForUrl(coordinate)
        let baseURL = NSURL(string: "https://api.forecast.io/forecast/\(apikey)/")
        let forecaseURL: NSURL = NSURL(string: coordinate, relativeToURL: baseURL)!
        let sharedSession = NSURLSession.sharedSession()
        let downloadTask: NSURLSessionDownloadTask? = sharedSession.downloadTaskWithURL(forecaseURL, completionHandler: completion)
        log.info("Start downloadning weather informaiton...")
        downloadTask!.resume()
    }
    
    static func showFaillAlertController(sender: UIViewController) {
        let nerworkIssueController = UIAlertController(title: "Error", message: "Unable to load data. Connectivity error!", preferredStyle: .Alert)
        let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
        nerworkIssueController.addAction(okButton)
        let cancelButton = UIAlertAction(title: "cancel", style: .Cancel, handler: nil)
        nerworkIssueController.addAction(cancelButton)
        sender.presentViewController(nerworkIssueController, animated: true, completion: nil)
    }
    
    
    static func convertFtoC(f: Int) -> Double {
        var celsius: Double = Double(f-32)
        celsius =  Double(celsius) / Double(1.8)
        return celsius
    }
    
    
    private static func coordinateAsStringForUrl(coordinate: CLLocationCoordinate2D) -> String{
        let latitude = String(stringInterpolationSegment: coordinate.latitude)
        let langtitude = String(stringInterpolationSegment: coordinate.longitude)
        let coordinate = latitude + "," + langtitude
        return coordinate
    }
    
}