//
//  ViewController.swift
//  Weather
//
//  Created by Jumpei Katayama on 10/26/14.
//  Copyright (c) 2014 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreLocation
import XCGLogger

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var weatherView: WeatherView!
    
    var didFindLocation: Bool!
    private lazy var locationManager: CLLocationManager = {
        let lm = CLLocationManager()
        // set delegate to receive location callbacks
        lm.delegate = self
        // status check
        //check locationStatus
//        print(CLLocationManager.locationServicesEnabled())
//        print(CLLocationManager.isRangingAvailable())
        return lm
    }()
    
    
    private func showLocationStatus(manager: CLLocationManager) {
        print(manager.description)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
//        showLocationStatus(locationManager)
        // Ask the user about using the location
        log.info("Checking locationAuthorization...")
        if locationManager.respondsToSelector("requestWhenInUseAuthorization") {
            log.info("AuthorizationStatus: \(CLLocationManager.authorizationStatus().description)")
            locationManager.requestWhenInUseAuthorization()
            log.info("SUCCESS: locationAuthorization")
        } else {
            log.info("FAIELED: locationAuthorization")
        }
        updateWeather()
    }
    
    
    func downloadCurrentWeather(coordinate: CLLocationCoordinate2D) {
        log.info("TRY: Download weather information")
        WeatherHelper.getCurrentWeatherData(coordinate, completion: {(location, repsponse, error) in
            guard error == nil else {// Error handling
                log.error("FAIL: Download wewther information")
                print(error)
                WeatherHelper.showFaillAlertController(self)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.updateUI()
                })
                return
            }

            let dataObject = NSData(contentsOfURL: location!)
            log.info("SUCESS: Download weather information")
            do {
                let weatherDictionary: NSDictionary = try NSJSONSerialization.JSONObjectWithData(dataObject!, options: []) as! NSDictionary
                let currentWeather = Current(weatherDictionary: weatherDictionary)
                log.info("SUCCESS: Create wether dictionary from JSON")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.updateUI(currentWeather)
                })
            } catch {
                log.error("FAIL: Create wether dictionary from JSON")
                print(error)
            }
        })
    }
    
    private func updateUI(newdata: Current? = nil) {
        log.info("Updating UI...")
        weatherView.updateUI(newdata)
        log.info("Updated UI")
    }
    

    /**
    Called when the app starts and the refresh button is tapped
    */
    private func updateWeather() {
        weatherView.refresh.hidden = true
        weatherView.indicator.hidden = false
        weatherView.indicator.startAnimating()
        locationUpdate()

    }
    
    private func locationUpdate() {
        log.info("Request Location update")
        didFindLocation = false
        locationManager.startUpdatingLocation()//fast but called many times
//        locationManager.requestLocation()//Slow
    }
    
    
    @IBAction func pushRefresh() {
        log.info("Tapped refreshButton")
        updateWeather()
    }
    
    // =================================
    // MARK: - CLLocationManagerDelegate
    // =================================
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !didFindLocation {
            didFindLocation = true
            locationManager.stopUpdatingLocation()
            print("STOP Updateing location")
            
            if locationManager.location != nil {
                // Location update should be called only when a new location has received
                downloadCurrentWeather(locationManager.location!.coordinate)
                let geocoder = CLGeocoder()
                log.info("Converting geolocation from \(locationManager.location!.coordinate)...")
                geocoder.reverseGeocodeLocation(locations.last!, completionHandler: {(placemarks, error) in
                    guard error == nil else { return }
                    self.weatherView.updateCityName(placemarks![0].locality!)
                    let myPlacemark = placemarks![0].locality!
                    log.info("SUCCESS: Converting geolocation \(locations.last!) to city name \(myPlacemark)")
                })
            }
            
        }
    }
    
//    *    Invoked when the authorization status changes for this application.
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        log.info("didChangeAuthorizationStatus callback")
        log.info("Authorization status has changed")
        switch status {
        case .AuthorizedAlways:
            log.info("AuthorizationStatus: AuthorizedAlways")
        case .AuthorizedWhenInUse:
            log.info("AuthorizationStatus: AuthorizedWhenInUse")
        case .Denied:
            log.info("AuthorizationStatus: Denied")
        case .NotDetermined:
            log.info("AuthorizationStatus: NotDetermined")
        case .Restricted:
            log.info("AuthorizationStatus: Restricted")
            
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error: \(error)")
        print("Locatoin manager: \(manager)")
    }

}

