//
//  CLLocationManager+Extensions.swift
//  Weather
//
//  Created by Jumpei Katayama on 1/3/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import Foundation
import CoreLocation

extension CLAuthorizationStatus {
    
    var description: String {
        get {
            switch(self) {
            case .AuthorizedAlways:
                return "AuthorizedAlways"
            case .AuthorizedWhenInUse:
                return "AuthorizedWhenInUse"
            case .Denied:
                return "Denied"
            case .NotDetermined:
                return "NotDetermined"
            case .Restricted:
                return "Restricted"
            }
        }
    }
}
